var stateObj = {
    foo: ""
};

function route_get_bak_next(route_get) {
    var getUrl = route.reduce(function(getUrl, route) {
        if (route.url == route_get) {
            return getUrl.concat(route)
        } else {
            return getUrl
        }
    }, []);
    return getUrl[0].template
}

function render_url(data) {
    app.empty();
    app.append($(data).html());
    document.title = template_link(data)
}

function render_url_default() {
    app.empty();
    window.location.hash = '/';
    render_back('/')
}

function renderUrlParam() {
    var gt_url = document.URL;
    var url_min = document.URL.replace("https://", '').replace("http://", '').replace("/#/", '')
    try {
        var urlparam = '/' + gt_url.split("#/").pop();
        render_back(urlparam)
    } catch (err) {
        var deface_url = gt_url.split("#/").pop()
        var host_https = 'https://' + location.host + '/';
        var host_http = 'http://' + location.host + '/';
        if (deface_url == host_http || deface_url == host_http || deface_url == '' || deface_url == ' ' || deface_url == location.host || url_min == location.host) {
            render_url_default()
        } else if (document.URL.replace("https://", '').replace("http://", '').replace("/#/", '') == location.host + "/") {
            render_url_default()
        } else {
            error_page()
        }
    }
}

function template_url(name) {
    var getUrl = route.reduce(function(getUrl, route) {
        if (route.url == name) {
            return getUrl.concat(route)
        } else {
            return getUrl
        }
    }, []);
    return getUrl[0].template
}

function render_back(linked) {
    var get_temp = template_url(linked)
    document.title = template_link(get_temp);
    $.get(template_dir + get_temp, function(result) {
        app.empty();
        app.append(result)
    })
}

function error_page() {
    app.empty();
    window.location.hash = error_url;
    history.replaceState(stateObj, "index", '/#' + error_url);
    document.title = error_title;
    $.get(template_dir + error_template, function(result) {
        app.append(result)
    })
}

function template_link(permalink) {
    var getUrl = route.reduce(function(getUrl, route) {
        if (route.template == permalink) {
            return getUrl.concat(route)
        } else {
            return getUrl
        }
    }, []);
    return getUrl[0].title
}

function template_name(permalink) {
    var getUrl = route.reduce(function(getUrl, route) {
        if (route.url == permalink) {
            return getUrl.concat(route)
        } else {
            return getUrl
        }
    }, []);
    return getUrl[0].name
}
jQuery(document).ready(function($) {
    renderUrlParam()
});
$(window).on('popstate', function(e) {
    var state = e.originalEvent.state;
    if (state !== null) {
        try {
            var doc_url = '/' + document.URL.split("#/").pop();
            render_back(doc_url)
        } catch (err) {
            error_page()
        }
    }
});